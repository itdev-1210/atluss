<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{

    public $timestamps = false;
    
    protected $fillable = [
        'advisor_id', 'name',
    ];

    protected $hidden = [
    ];

    public function advisor()
    {
        return $this->belongsTo('App\User', 'advisor_id');
    }

    public function advisor_client_group()
    {
        return $this->hasMany('App\AdvisorClient');
    }

    public function invites()
    {
        return $this->hasMany('App\Invite');
    }
}
