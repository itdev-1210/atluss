<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarSetting extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'user_id', 'daily_content', 'calendar_setting',
    ];
    
    protected $hidden = [
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
