<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DailyEmail extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    public $advisor;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($advisor)
    {
        $this->advisor = $advisor;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $title = "Daily Schedule for " . $this->advisor->name;
        $stock = (empty($this->advisor->emailSetting) || empty($this->advisor->emailSetting->daily_content)) ? 
                    'Atluss daily mail' : $this->advisor->emailSetting->daily_content;
        $action = $this->advisor->name.'\'s Schedule';
        $url = url("calendar/".$this->advisor->id);
        return (new MailMessage)
                    ->subject($title)
                    ->line($stock)
                    ->action($action, $url)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }

    /**
     * Determine the time at which the job should timeout.
     *
     * @return \DateTime
     */
    public function retryUntil()
    {
        return now()->addSeconds(5);
    }

}
