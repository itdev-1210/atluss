<?php

namespace App\Notifications;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReminderEmail extends Notification implements ShouldQueue
{
    use Queueable;

    public $appointment;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $types = config('constant.type_meeting');
        $start = Carbon::createFromFormat('Y-m-d H:i:s', $this->appointment->start);
        $end = Carbon::createFromFormat('Y-m-d H:i:s', $this->appointment->end);
        $stock = (empty($this->appointment->advisor->emailSetting) || empty($this->appointment->advisor->emailSetting->reminder_content)) ? 
                    'Appointment Reserved' : $this->appointment->advisor->emailSetting->reminder_content;                    
        $action = $this->appointment->client->name.'\'s Appointment';
        $url = url("calendar/".$this->appointment->advisor->id);       
        $agency_name = $this->appointment->advisor->clients()->find($this->appointment->client_id)->pivot->agency->name;
        return (new MailMessage)
                    ->line($stock)
                    ->action($action, $url)
                    ->line('Appointment Time: '.$start->format('n/d/Y h:i:s A').' ~ '.$end->format('n/d/Y h:i:s A'))
                    ->line('Agency Name: '.$agency_name)
                    ->line('Appointment Type: '.$types[$this->appointment->type])
                    ->line('Appointment Set By: '.$this->appointment->setby)
                    ->line('Contact Name: '.$this->appointment->name)
                    ->line('Contact Address: '.$this->appointment->address)
                    ->line('Contact City: '.$this->appointment->city)
                    ->line('Contact Zip Code: '.$this->appointment->zip_code)
                    ->line('Contact State: '.$this->appointment->state)
                    ->line('Contact Phone Number: '.$this->appointment->phone_number)
                    ->line('Contact Email: '.$this->appointment->email)
                    ->line('Reason for Appointment/Details: '.$this->appointment->description)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
