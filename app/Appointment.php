<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'start', 'end', 'setby', 'name', 'address', 'phone_number', 'email', 'state', 'zip_code',
        'type', 'description', 'advisor_id', 'client_id', 'status', 'length', 'city',
    ];

    protected $hidden = [
        'deleted_at',
    ];

    protected $dates = ['deleted_at'];

    public function advisor()
    {
        return $this->belongsTo('App\User', 'advisor_id');
    }

    public function client()
    {
        return $this->belongsTo('App\User', 'client_id');
    }
}
