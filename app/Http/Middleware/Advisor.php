<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Advisor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->isAdvisor()) {
                return $next($request);
            }
        }
        
        return redirect('home');
    }
}
