<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ChangePasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Change Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the password change view for the given token.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showChangeForm(Request $request)
    {
        if ($this->guard()->user()->isAdmin()) {
            return view('admin.auth.passwords.change');
        }
        return view('auth.passwords.change');
    }

    /**
     * Get the password change validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'current_password' => 'required|string|min:6',
            'password' => 'required|confirmed|min:6',
        ];
    }

    /**
     * Get the password change credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'current_password', 'password', 'password_confirmation'
        );
    }

    /**
     * Change the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function change(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());
        $user = $this->guard()->user();
        if (Hash::check($request->current_password, $user->password)) {
            $user->password = Hash::make($request->password);    
            $user->setRememberToken(Str::random(60));    
            $user->save();    
            event(new PasswordReset($user));    
            $this->guard()->login($user);
            return back()->with('success', 'Password changed');
        } else {
            return back()->withInput($request->only('current_password'))
                        ->withErrors(['current_password' => 'Invalid password']);
        }
    }

    /**
     * Get the password change validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}
