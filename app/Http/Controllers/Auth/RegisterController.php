<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Invite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
// use Illuminate\Foundation\Auth\RegistersUsers;
use Bestmomo\LaravelEmailConfirmation\Traits\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'user/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function redirectTo() {
        $user = $this->guard()->user();
        if ($user->isAdvisor()) {
            if ($user->profile !== NULL) {
                return 'dashboard';
            }
        } elseif ($user->isAdmin()) {
            return 'admin/dashboard';
        }
        
        return $this->redirectTo;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'company' => 'required|string|max:255',
            'g-recaptcha-response' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data, $type = 2)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'company' => $data['company'],
            'type' => $type,
        ]);
    }

    public function accept(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'invite_id' => 'required|integer|exists:invites,id',
            'name' => 'required|string|max:255|unique:users',
            'email' => 'required|string|email|max:255|unique:users|exists:invites',
            'password' => 'required|string|min:6|confirmed',
            'company' => 'required|string|max:255',
            'g-recaptcha-response' => 'required',
        ]);
        $validator->validate();

        $invite = Invite::find($request->invite_id);
        $invite->delete();

        $user = $this->create($request->all(), $invite->user->isAdvisor() ? 3 : 2);
        $user->confirmed = true;
        $user->save();

        if ($invite->user->isAdvisor()) {
            $user->advisors()->attach($invite->user, ['agency_id' => $invite->agency_id,]);
        }

        event(new Registered($user));

        $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: ($user->isClient() ? redirect("client/calendar/$invite->user_id") : redirect($this->redirectPath()));
    }
    
    public function changeEmail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
        ]);
        $validator->validate();

        if ($request->session()->has('user_id')) {

            $model = config('auth.providers.users.model');

            $user = $model::findOrFail($request->session()->get('user_id'));
            // $user = Auth::user();;
            if (empty($user->confirmation_code)) {
                $user->email = $request['email'];
                $user->confirmation_code = str_random(30);
                $user->confirmed = false;
                $user->save();
            }

            $this->notifyUser($user);
            
            return redirect(route('login'))->with('confirmation-success', trans('confirmation::confirmation.message'));
        }

        return redirect(route('home'));
    }
}
