<?php

return [
    'register_accept' => 'Thanks for accepting! Please sign up.',
    'login_accept' => 'Thanks for accepting! Please login.',
    'success' => 'Invitation email successfully sent. Please check email.',
    'resend' => 'Invitation email successfully sent again. Please check email.',
    'invalid_invite' => 'Invalid invitation',
];
