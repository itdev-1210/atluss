import Graph from './components/Graph.vue'
// Vue.component("graph-component", require("./components/Graph.vue"));

const graph = new Vue({
    el: '.container',
    components: {
        'graph-component': Graph
    },
    created() {
      console.log("ChatComponent mounted.");
    }
});