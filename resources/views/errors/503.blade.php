@extends('errors.page')

@section('content')
<h1>Scheduled Maintenance</h1>

<p>
	We are under a scheduled maintenance and we'll be back shortly!
</p>
@stop
