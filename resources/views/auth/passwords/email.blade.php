@extends('basic.page') 
@section('title_postfix', 'Reset') 
@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop 
@section('banner')
<div class="container">
</div>
@stop 
@section('content')
<section class="section-atluss">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="pricing-frame">
                    <form action="{{ url('password/email') }}" method="post" id="reset-form" class="login-form">
                        {!! csrf_field() !!}
                        <div class="heading text-center">Reset Password</div>
                        <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                            <input type="email" class="form-control" placeholder="Email" name="email" id="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-black round btn-login">Reset Password >></button>
                        </div>
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@stop 
@section('feature') 
@stop