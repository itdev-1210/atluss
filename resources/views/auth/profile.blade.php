@extends('basic.page') 
@section('title_postfix', 'Profile') 
@section('header')
<div class="container-fluid text-center">
  <div class="header_text">Welcome</div>
</div>
@stop 
@section('banner')
<div class="container text-center">
</div>
@stop 
@section('content')
<section class="section-atluss">
  <div class="container">
    <form action="{{ url('user/profile/set') }}" method="post" id="subscribe-form" class="pricing-frame">
      {!! csrf_field() !!}
      <input type="hidden" id="timezone" name="timezone">
      <input type="hidden" id="monthly" name="monthly" value="{{ $monthly }}">
      <div class="stripe-errors {{ $err != '' ? 'alert alert-danger' : '' }}">{{ $err }}</div>
      <div class="row">
        <div class="col-sm-6 ">
          <div class="login-form">
            <div class="heading text-center">Credit Card Info</div>
            <div class="form-group {{ $errors->has('card_name') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="Name on Card" id="card_name" name="card_name"
              @if (old('card_name') !== null)
              value="{{ old('card_name') }}" 
              @elseif ($profile['card_name'] !== null)
              value="{{ $profile['card_name'] }}" 
              @endif
              required>
              @if ($errors->has('card_name'))
              <span class="help-block">
                  <strong>{{ $errors->first('card_name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('card_number') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="Card Number" id="card_number" name="card_number"
              @if (old('card_number') !== null)
              value="{{ old('card_number') }}" 
              @elseif ($profile['card_number'] !== null)
              value="{{ $profile['card_number'] }}" 
              @endif
              required>
              @if ($errors->has('card_number'))
              <span class="help-block">
                  <strong>{{ $errors->first('card_number') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('expires_at') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="MM/YY" id="expires_at" name="expires_at"
              @if (old('expires_at') !== null)
              value="{{ old('expires_at') }}" 
              @elseif ($profile['expires_at'] !== null)
              value="{{ $profile['expires_at'] }}" 
              @endif
              required>
              @if ($errors->has('expires_at'))
              <span class="help-block">
                  <strong>{{ $errors->first('expires_at') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('cvc') ? 'has-error' : '' }}">
              <input type="text" maxlength="3" class="form-control" placeholder="CVC" id="cvc" name="cvc"
              @if (old('cvc') !== null)
              value="{{ old('cvc') }}" 
              @elseif ($profile['cvc'] !== null)
              value="{{ $profile['cvc'] }}" 
              @endif
              required>
              @if ($errors->has('cvc'))
              <span class="help-block">
                  <strong>{{ $errors->first('cvc') }}</strong>
              </span>
              @endif
            </div>
          </div>
        </div>
        <div class="col-sm-6 ">
          <div class="login-form">
            <div class="heading text-center">Billing Address</div>
            <div class="form-group {{ $errors->has('user_name') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="Name" id="user_name" name="user_name"
              @if (old('user_name') !== null)
              value="{{ old('user_name') }}" 
              @elseif ($profile['user_name'] !== null)
              value="{{ $profile['user_name'] }}" 
              @endif
              required>
              @if ($errors->has('user_name'))
              <span class="help-block">
                  <strong>{{ $errors->first('user_name') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="Address" id="address" name="address"
              @if (old('address') !== null)
              value="{{ old('address') }}" 
              @elseif ($profile['address'] !== null)
              value="{{ $profile['address'] }}" 
              @endif
              required>
              @if ($errors->has('address'))
              <span class="help-block">
                  <strong>{{ $errors->first('address') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="City" id="city" name="city"
              @if (old('city') !== null)
              value="{{ old('city') }}" 
              @elseif ($profile['city'] !== null)
              value="{{ $profile['city'] }}" 
              @endif
              required>
              @if ($errors->has('city'))
              <span class="help-block">
                  <strong>{{ $errors->first('city') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
              <select class="form-control" id="state" name="state" required>
                @foreach ($states as $state)
                <option value="{{ $state->id }}"
                  @if (old('state') == $state->id) selected 
                  @elseif ($profile['state'] == $state->name) selected 
                  @endif>{{ $state->name }}</option>
                @endforeach
              </select>
              @if ($errors->has('state'))
              <span class="help-block">
                  <strong>{{ $errors->first('state') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : '' }}">
              <input type="text" class="form-control" placeholder="Zip Code" id="zip_code" name="zip_code"
              @if (old('zip_code') !== null)
              value="{{ old('zip_code') }}" 
              @elseif ($profile['zip_code'] !== null)
              value="{{ $profile['zip_code'] }}" 
              @endif
              required>
              @if ($errors->has('zip_code'))
              <span class="help-block">
                  <strong>{{ $errors->first('zip_code') }}</strong>
              </span>
              @endif
            </div>
          </div>
        </div>
      </div>
      <div class="text-center">
        <button type="button" onClick="createStripe()" class="btn btn-black round btn-login">Submit >></button>
      </div>
    </form>
  </div>
</section>
@stop 
@section('feature') 
@stop
@push('js')
<script src="https://js.stripe.com/v2/"></script>
<script>
  Stripe.setPublishableKey('{{ config('services.stripe.key') }}');
  let timeoffset = new Date().getTimezoneOffset();
  $('#timezone').val(timeoffset)
  // $('#expires_at').datepicker({ 
  //     autoclose: true,
  //     startDate: '+1d'
  // });
  $('#expires_at').mask("99/99");
  let expire = $('.expiry_value').attr('value');

  function createStripe() {
    var form = $('#subscribe-form');
    var card_number = $('#card_number').val();
    var expire_data = $('#expires_at').val();
    var exps = expire_data.split('/');
    var cvc = $('#cvc').val();

    if (!Stripe.card.validateCardNumber(card_number)) {
      form.find('.stripe-errors').text('Card Number is Invalid').addClass('alert alert-danger');
      return;
    } else if (!Stripe.card.validateCVC(cvc)) {
      form.find('.stripe-errors').text('CVC is Invalid').addClass('alert alert-danger');
      return;
    } else if (!Stripe.card.validateExpiry(exps[0], exps[1])) {
      form.find('.stripe-errors').text('Card is expire').addClass('alert alert-danger');
      return;
    }
    // disable the form button
    form.find('button').prop('disabled', true);

    // use the stripe library. create a single use token
    Stripe.card.createToken({
      number:card_number,
      cvc:cvc,
      exp_month:exps[0],
      exp_year:exps[1],
      }, function(status, response) {

      // if there are errors, show them
      if (response.error) {
        form.find('.stripe-errors').text('Invalid Card Infomation').addClass('alert alert-danger');
        form.find('button').prop('disabled', false);
      } else {

        // if there are no errors...
        console.log(response);

        // append the token to the form
        form.append($('<input type="hidden" name="stripeToken">').val(response.id));

        // submit the form
        form.get(0).submit();
      }
    });
  }

</script>
@endpush