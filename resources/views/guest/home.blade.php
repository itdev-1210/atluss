@extends('basic.page')

@section('title_postfix', 'Home')

@section('header')
<div id="carousel-slider" class="carousel slide carousel-fade" data-ride="carousel" data-interval="3000">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active clearfix">
      <img src="img/header-back.png">
      <div class="carousel-caption">
        <div class="container">
          <div class="col-xs-12 col-lg-6 col-md-5 col-sm-5 pull-right">
            <img src="{{ asset('/img/desktop.png') }}" class="img-responsive" alt="destop-image">
          </div>
          <div class="col-lg-5 col-md-6 col-sm-6">
            <p class="title">Virtual Connection Toolkit</p>
            <p class="content">Atluss solves your appointment scheduling challenges with a tool so exceedingly powerful, yet so easy to use that you will quickly wonder how you ever managed without it.</p>
            <a class="btn btn-black round btn-login" href="{{ url('about') }}" style="color: white">Learn More >></a>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Left and right controls -->
  <a class="left carousel-control" href="#carousel-slider" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
      </a>
  <a class="right carousel-control" href="#carousel-slider" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
      </a>
</div>
@stop

@section('banner')
<div class="container text-center">
  <h2>Why You’ll Love Atluss</h2>
  <p>
    Atluss takes a proven appointment scheduling system and turns it into an automated tool that’s exceedingly powerful, yet easy to use. With Atluss you share your calendar through a personal URL, allowing your partner agencies and staff (users) to schedule appointments in real time, based on your preferences and ever-changing availability. Atluss gives you complete control to manage and organize your schedule in the way that keeps you working at your peak productivity.
  </p>
  </div>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-7 col-lg-7 home-img">
        <img src="{{ asset('/img/home.png') }}" class="img-responsive" alt="mob-img">
      </div>
      <div class="col-sm-6 col-md-5 col-lg-4 col-lg-offset-1 text-center home-text">
        <h2>Atluss Features</h2>
        <ol>
          <li>Manages your schedule and automatically tracks appointments by user and customer</li>
          <li>Keeps your need for new customers and appointments a priority for your partner agencies (users)</li>
          <li>Increase the number of appointments set without cold-calling</li>
        </ol>
        <a class="btn btn-black round btn-login" href="{{ url('about') }}">Learn More</a>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
<div class="track" style="background-image:url(img/track.png);background-size:cover;background-position:center;">
  <div class="container text-center">
    <h2>A better way of staying on track</h2>
    <div class="col-sm-4">
      <div class="fa fa-calendar"></div>
      <p>Automatically informs users of updates and changes instantly to make you more efficient</p>
      <a class="btn round btn-login" href="{{ url('about') }}">Learn More >></a>
    </div>
    <div class="col-sm-4">
      <div class="fa fa-clock-o"></div>
      <p>Sends reminders based on your preferences, cutting down on no-shows</p>
      <a class="btn round btn-login" href="{{ url('about') }}">Learn More >></a>
    </div>
    <div class="col-sm-4">
      <div class="fa fa-users"></div>
      <p>Automated scheduling makes organization simple and effective with multiple users across multiple agencies</p>
      <a class="btn round btn-login" href="{{ url('about') }}">Learn More >></a>
    </div>
  </div>
</div>
@stop

@push('js')
<script>
    $(function() {      
        /* slider fade index page */
        $(".carousel-fade").carousel({
          interval: 0
        });
    })
</script>
@endpush