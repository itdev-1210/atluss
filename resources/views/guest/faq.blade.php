@extends('basic.page')

@section('title_postfix', 'About')

@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Faq's</div>
</div>
@stop

@section('banner')
<div class="container text-center">

</div>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    @forelse ($faqs as $faq)
    <div class="faq_list">
      <p class="question">{{ $faq->question }}</p>
      <p class="answer">{{ $faq->answer }}</p>
    </div>
    @empty
    @endforelse
  </div>
</section>
@stop

@section('feature')
@guest
<div class="container">
    <div class="feature text-center">
        <div class="container">
            <h3>Sign Up Now to Get started!</h3>
            <a class="btn round btn-login" href="{{ route('register') }}">Sign up >></a>
        </div>
    </div>
</div>
@endguest
@auth
<div class="container">
    <div class="feature text-center">
        <div class="container">
            <h3>Now to Get started!</h3>
            <a class="btn round btn-login" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>
        </div>
    </div>
</div>
@endauth
@stop