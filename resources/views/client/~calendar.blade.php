@extends('template')

@push('css')
@endpush

@section('title_postfix', 'Calendar')

@section('content')
<div class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form">
        <div class="heading text-center">
          {{ $user->name }}'s Calendar
          <br>
          <div class="text-center btn-group">
            <button type="button" class="btn btn-action" value="day">Day</button>
            <button type="button" class="btn btn-action" value="week">Week</button>
            <button type="button" class="btn btn-action active" value="month">Month</button>
            <button type="button" class="btn btn-action" value="year">Year</button>
          </div>
        </div>
        <!-- THE CALENDAR -->
        <div id="calendar"></div>
        <div class="">
            <h3>New Appointment</h3>
          <input type="hidden" name="advisor_id" id="advisor_id"  value="{{ $user->id }}">
          <div class="row">
              <div class="col-sm-6">
                  <div class="form-group">
                    <label>Appointment Date</label>
                    <input type="text" class="form-control" placeholder="mm/dd/yyyy" id="appointment_date" name="appointment_date">
                  </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                    <label>Appointment Time</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                        </div>
                        <input type="text" class="form-control" id="start_time" placeholder="08:00" name="start_time">
                    </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Agency name</label>
                <input type="text" class="form-control" id="agency" name="agency" placeholder="Name" value="{{ $user->name }}" readonly>
              </div>
              <div class="form-group">
                <label>Appointment Set By:</label>
                <input type="text" class="form-control" id="setby" name="setby" placeholder="Name" value="{{ Auth::user()->name }}" readonly>
              </div>
              <div class="form-group">
                <label>Contact Name</label>
                <input type="text" class="form-control" id="name" name="name" placeholder="Contact Name">
              </div>
              <div class="form-group">
                <label>Contact Address</label>
                <input type="text" class="form-control" id="address" name="address" placeholder="Contact Address">
              </div>
              <div class="form-group">
                <label>Contact Phone number</label>
                <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Contact Phone number">
              </div>
              <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label>Contact Email</label>
                <input type="text" class="form-control" id="email" name="email" placeholder="Contact Email">
              </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                  <label>Length of Meeting</label>
                  <select class="form-control" id="length" name="length">
                      @foreach($lengths as $length)
                      <option value="{{ $length['value'] }}">{{ $length['name'] }}</option>
                      @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label>Appointment Type</label>
                  <select class="form-control" id="type" name="type">
                      @foreach($types as $type)
                      <option value="{{ $loop->index }}">{{ $type }}</option>
                      @endforeach
                  </select>
                </div>
                <div class="form-group">
                    <label>Reason for Appointment/Details</label>
                    <textarea class="form-control" rows="12" id="description" placeholder="Reason for Appointment/Details..." name="description"></textarea>
                </div>
            </div>
          </div>
          <div class="text-center">
              <button type="submit" class="btn btn-action btn-add">Add Appointment</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('feature')
@stop

@push('js')
<script>
//Timepicker
$('#start_time').timepicker({ 
  showInputs: false, 
  defaultTime: '08:00 AM', 
  minuteStep: 15, 
  showMeridian: true,
  maxHours: 24, 
});
$('#appointment_date').datepicker({ 
    autoclose: true,
    startDate: 'today'
});
// init calendar
var date = new Date() 
var d = date.getDate(), m = date.getMonth(), y = date.getFullYear() 
$('#calendar').fullCalendar({
  header : { left: 'title', center: '', right : 'prev,next today' }, 
  // header : { left : 'title', center: 'agendaDay,agendaWeek,month,listYear', right : 'prev,next today' }, 
  buttonText: { today: 'today', month: 'Month', week : 'Week', day : 'Day', list: 'Year' },
  defaultView: 'month',  
  editable  : true,
  droppable : true, // this allows things to be dropped onto the calendar !!!  
  eventLimit: true,
  displayEventEnd: true,
  weekends: true,
  businessHours: {
    // days of week. an array of zero-based day of week integers (0=Sunday)
    dow: [ 1, 2, 3, 4, 5 ], // Monday - Thursday
    start: '08:00', // a start time (10am in this example)
    end: '20:00', // an end time (6pm in this example)
  },
  timeFormat: 'H:mm', // uppercase H for 24-hour clock
  views: {
    month: {
        // options apply to basicDay and agendaDay views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    week: {
        // options apply to basicWeek and agendaWeek views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    day: {
        // options apply to basicDay and agendaDay views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    }
  },
//  timezone: 'America/Chicago',//'UTC',
//  timezone: 'locale',
  visibleRange: function(currentDate) {
    return {
      start: currentDate.clone().startOf('day'),
      end: currentDate.clone().add(1, 'months')
    };
  },
  validRange: function(nowDate) {
    return {
      start: nowDate.clone().startOf('day'),
      end: nowDate.clone().add(1, 'months')
    };
  },
  events: function(start, end, timezone, callback) { // load events
    if (window.axios) {
        let advisor_id = $('#advisor_id').val();
      const url = '/client/appointment/search/' + advisor_id; 
      let timeoffset = new Date().getTimezoneOffset();
      const data = { 
        start: start.unix(),//format(), 
        end: end.unix(),//format(),
        timezone: timeoffset,
      };
    console.log(data)
      axios.post(url, data) 
      .then(function(response) {
        var events = [];
    console.log(response.data)
        if (layer && response.data.success) {
          //window.layer.msg('Created new Appointment');
          for (let event of response.data.appointments) {
            events.push(event);
          }
        }
        console.log(events);
        callback(events);
      }) 
      .catch(function(error) {
        callback([]);
        console.log(error.response.data)
        if (layer) layer.msg(error.response.data.message);
      });
    }
  },
  drop      : function (date, allDay) { // this function is called when something is dropped
    // retrieve the dropped element's stored Event Object
    var originalEventObject = $(this).data('eventObject')
    // we need to copy it, so that multiple events don't have a reference to the same object
    var copiedEventObject = $.extend({}, originalEventObject)
    var start_time = $('#start_time').val().split(":");
    var end_time = $('#end_time').val().split(":");
    var start_hour = start_time[0];
    var start_minute = start_time[1];
    var end_hour = end_time[0];
    var end_minute = end_time[1];
    var start_date = date;
    var end_date = date.clone();
    start_date.utc().hours(start_hour);
    start_date.utc().minutes(start_minute);
    end_date.utc().hours(end_hour);
    end_date.utc().minutes(end_minute);
    console.log(start_date)
    console.log(end_date)
    console.log('originalEventObject')
    console.log(originalEventObject)
    console.log('copiedEventObject')
    console.log(copiedEventObject)
    console.log(date)
    console.log(allDay)
    // assign it the date that was reported
    copiedEventObject.start           = start_date
    copiedEventObject.end           = end_date
    copiedEventObject.allDay          = allDay
    copiedEventObject.backgroundColor = $(this).css('background-color')
    copiedEventObject.borderColor     = $(this).css('border-color')
    copiedEventObject.textColor     = $(this).css('color')
    if (window.axios) {
      const url = '/appointment/create'; 
      const data = { 
        title: copiedEventObject.title, 
        start: copiedEventObject.start.format(),
        end: copiedEventObject.end ? copiedEventObject.end.format() : null,
        allDay: true,//copiedEventObject.allDay,
        backgroundColor: copiedEventObject.backgroundColor,
        borderColor: copiedEventObject.borderColor,
        textColor: copiedEventObject.textColor,
      }; 
      console.log(data)
      axios.post(url, data) 
      .then(function(response) {
          if (layer && response.data.success) {
            copiedEventObject.id = response.data.appointment
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)
            window.layer.msg('Created new Appointment');
          }
      }) 
      .catch(function(error) {
        console.log(error.data)
          if (layer) layer.msg(error.data);
      });
    }  
    // is the "remove after drop" checkbox checked?
    if ($('#drop-remove').is(':checked')) {
      // if so, remove the element from the "Draggable Events" list
      $(this).remove()
    }
  },
  eventDrop: function(event, delta, revertFunc) {
      if (!confirm("Are you sure about this change?")) {
          revertFunc();
      } else {
        if (window.axios) {
          const url = '/appointment/update'; 
          const data = { 
            id: event.id,
            title: event.title, 
            start: event.start.format(),
            end: event.end ? event.end.format() : null,
            allDay: event.allDay,
            backgroundColor: event.backgroundColor,
            borderColor: event.borderColor,
            textColor: event.textColor,
          }; 
          console.log(data)
          axios.post(url, data) 
          .then(function(response) {
            if (layer && response.data.success) window.layer.msg('Updated Appointment');
          }) 
          .catch(function(error) {
            if (layer) layer.msg(error.data);
          });
        }        
      }
  },
  eventResize: function(event, delta, revertFunc) {
      if (!confirm("is this okay?")) {
          revertFunc();
      } else {
        const url = '/appointment/update'; 
        const data = { 
          id: event.id,
          title: event.title, 
          start: event.start.format(),
          end: event.end ? event.end.format() : null,
          allDay: event.allDay,
          backgroundColor: event.backgroundColor,
          borderColor: event.borderColor,
          textColor: event.textColor,
        }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          if (layer && response.data.success) window.layer.msg('Updated Appointment');
        }) 
        .catch(function(error) {
          if (layer) layer.msg(error.data);
        });
      }
  },
  dayClick: function(date, jsEvent, view) {
    $('#appointment_date').val(date.format('MM/DD/YYYY'));
    //$(this).css('background-color', 'red')
    //alert(view.name);
  },
  eventClick: function(event, element) {
    //alert('a event has been clicked!');
    console.log(event.start);
    $('#appointment_date').val(event.start.format('MM/DD/YYYY'));
    $('#start_time').val(event.start.format('H:mm'))
  }
})
$('.btn-group .btn-action').click(function() {
  $('.btn-action').removeClass('active');
  $(this).addClass('active');
  switch($(this).val()) {
    case 'day':
    $('#calendar').fullCalendar('changeView', 'agendaDay');
    break;
    case 'week':
    $('#calendar').fullCalendar('changeView', 'agendaWeek');
    break;
    case 'month':
    $('#calendar').fullCalendar('changeView', 'month');
    break;
    case 'year':
    $('#calendar').fullCalendar('changeView', 'listYear');
    break;
  }
})
$('.btn-add').click(function() {
    let type = ['Office Meeting', 'Phone Meeting'];
  let date = moment($('#appointment_date').val(), 'MM/DD/YYYY');
  let start_time = moment($('#start_time').val(), 'H:mm');
  let start_hour = start_time.hour();
  let start_minute = start_time.minute();
  let length = $('#length').val();
  let end_time = start_time.add(length, 'minutes');
  let end_hour = end_time.hour();
  let end_minute = end_time.minute();
  var start_date = date;
  var end_date = date.clone();
  start_date.hour(start_hour);
  start_date.minute(start_minute);
  end_date.hour(end_hour);
  end_date.minute(end_minute);
  console.log(start_date)
  console.log(end_date)
  let agency = $('#agency').val();
  let setby = $('#setby').val();
  let name = $('#name').val();
  let address = $('#address').val();
  let phone_number = $('#phone_number').val();
  let email = $('#email').val();
  let type = $('#type').val();
  let description = $('#description').val();
  if (window.axios) {
      let advisor_id = $('#advisor_id').val();
      const url = '/client/appointment/create'; 
    const data = { 
      advisor_id: advisor_id,
      start: start_date.unix(),
      end: end_date.unix(),
      agency: agency,
      setby: setby,
      name: name,
      phone_number: phone_number,
      email: email,
      type: type,
      description: description,
    }; 
    console.log(data)
    axios.post(url, data) 
    .then(function(response) {
        if (layer && response.data.success) {
          data.id = response.data.appointment
          // render the event on the calendar
          // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
          $('#calendar').fullCalendar('renderEvent', data, true)
          window.layer.msg('Created new Appointment');
        }
    }) 
    .catch(function(error) {
      console.log(error.data)
        if (layer) layer.msg(error.data);
    });
  }
})
</script>
@endpush