@extends('template')

@push('css')
<style>
  .fc-day.fc-widget-content[data-date="{{$start_day->format('Y-m-d')}}"] {
    background: rgba(30, 144, 255, 0.3);
  }
  textarea#description {
    min-height: 116px;
  }
  .available td {
    color: #762696; 
  }
  .unavailable td {
    color: #be0000; 
  }
  .phone td {
    color: #00cced; 
  }
  .office td {
    color: #000000; 
  }
</style>
@endpush
@section('title_postfix', 'Calendar')

@section('content')
<div class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="calendar-form">
        <div class="">
          <ul class="events pull-right">
            <li>Office Meeting Scheduled</li>
            <li>Unavailable for Appointment</li>
            <li>Available for Appointment</li>
            <li>Phone Meeting Scheduled</li>
          </ul>
        </div>
        <div class="heading text-center">
          {{ $advisor->name }}'s Calendar
          <br>
          <div class="text-center btn-group">
            <button type="button" class="btn btn-action @if ($mode == 'day') active @endif" value="day">Day</button>
            <button type="button" class="btn btn-action @if ($mode == 'week') active @endif" value="week">Week</button>
            <button type="button" class="btn btn-action @if ($mode == 'month') active @endif" value="month">Month</button>
            <button type="button" class="btn btn-action @if ($mode == 'year') active @endif" value="year">Year</button>
          </div>
          <input type="hidden" name="advisor_id" id="advisor_id"  value="{{ $advisor->id }}">
        </div>
        <br>
        <div class="pull-left">
          <h3 id="date-title">
            @if($mode=='year'){{ $start_day->format('Y') }}
            @elseif($mode=='month'){{ $start_day->format('F Y') }}
            @elseif($mode=='week'){{ $start_day->format('F j,Y') }}
            @else{{ $start_day->format('F j,Y') }}
            @endif
          </h3>
          <input type="hidden" id="timestamp" value="{{ $start_day->format('U') }}"/>
        </div>
        <div class="text-center btn-group pull-right" id="arrow-group">
          <button type="button" class="btn btn-action btn-back" id="left-arrow"><span class="fc-icon fc-icon-left-single-arrow"></span></button>
          <button type="button" class="btn btn-action btn-back" id="btn-today">today</button>
          <button type="button" class="btn btn-action btn-back" id="right-arrow"><span class="fc-icon fc-icon-right-single-arrow"></span></button>
        </div>
        <!-- THE CALENDAR -->
        <div id="calendar"></div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true" style="margin-top: 16px;">
          @forelse ($appointments as $appointment)
          <div class="panel panel-warning">
            <!-- Appointment by Advisor -->
            @if (empty($appointment->client_id))
            <div class="panel-heading" role="tab" id="headingTwo">
              <h4 class="panel-title @if ($appointment->type==10 || $appointment->type==12) text-purple @elseif ($appointment->type==11 || $appointment->type==13) text-red @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="false"
                  aria-controls="collapse-{{ $loop->index }}">
                  {{ $appointment->start->format('g:i A') }}&nbsp;~&nbsp;{{ $appointment->end->format('g:i A') }}
                  <span>{{ $appointment->title }}</span>
                </a>
              </h4>
            </div>
            <div id="collapse-{{ $loop->index }}" class="panel-collapse collapse @if ((old('active_id') === NULL && $appointment->start->format('U') == $timeStamp) || (old('active_id') != NULL && $loop->index == old('active_id'))) in @endif" role="tabpanel" aria-labelledby="headingTwo">
              @if ($appointment->type == 10 || $appointment->type ==  12)
              <form action="{{ url('client/appointment/create') }}" method="post" id="appointment-form-{{ $loop->index }}" class="">
                {!! csrf_field() !!}
                <input type="hidden" name="advisor_id" id="advisor_id"  value="{{ $advisor->id }}">
                <input type="hidden" name="active_id" id="active_id"  value="{{ $loop->index }}">
                <input type="hidden" class="form-control text" id="appointment_date" name="appointment_date" value="{{ $start_day->format('m/d/Y') }}" >
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('start_time') ? 'has-error' : '' }}">
                      <label>Appointment Time</label>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="col-xs-6">
                            <div class="row">
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-clock-o"></i>
                                </div>
                                <input type="text" class="form-control" id="start_time" placeholder="08:00" name="start_time"
                                @if (old('start_time') !== null)
                                value="{{ old('start_time') }}"
                                @else
                                value="{{ $appointment->start->format('g:i A') }}"
                                @endif required>
                              </div>
                            </div>
                          </div>
                          <div class="col-xs-6">
                            <div class="row">
                              <div class="input-group">
                                <div class="input-group-addon">
                                  <i class="fa fa-calendar-o"></i>
                                </div>
                                <input type="text" class="form-control" id="start_date" value="{{ $appointment->start->format('m/d') }}" readonly>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      @if ($errors->has('start_time'))
                      <span class="help-block">
                        <strong>{{ $errors->first('start_time') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group">
                      <label>Agency Name</label>
                      <input type="text" class="form-control" id="agency" name="agency" placeholder="Agency Name" value="{{ $advisor->pivot->agency->name }}" readonly required>
                      @if ($errors->has('setby'))
                      <span class="help-block">
                        <strong>{{ $errors->first('setby') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                      <label>Appointment Type</label>
                      <select class="form-control" id="type" name="type" required>
                        <option value="">Select Appointment Type</option>
                        @foreach($types as $type)
                        <option value="{{ $loop->index }}" @if(old('type')!=NULL && old('type')==$loop->index) selected @endif>{{ $type }}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('type'))
                      <span class="help-block">
                        <strong>{{ $errors->first('type') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('length') ? 'has-error' : '' }}">
                      <label>Length of Meeting</label>
                      <select class="form-control" id="length" name="length" required>
                        <option value="">Select Meeting Length</option>
                        @foreach($lengths as $length)
                        <option value="{{ $loop->index }}" @if(old('length')!=NULL && old('length')==$loop->index) selected @endif>{{ $length }}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('length'))
                      <span class="help-block">
                        <strong>{{ $errors->first('length') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('setby') ? 'has-error' : '' }}">
                      <label>Appointment Set By:</label>
                      <input type="text" class="form-control" id="setby" name="setby" placeholder="Name" value="{{ Auth::user()->name }}" readonly required>
                      @if ($errors->has('setby'))
                      <span class="help-block">
                        <strong>{{ $errors->first('setby') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                      <label>Contact Name</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ old('name') }}" required>
                      @if ($errors->has('name'))
                      <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                      <label>Contact Address</label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="Address" value="{{ old('address') }}" required>
                      @if ($errors->has('address'))
                      <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                      <label>Contact City</label>
                      <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ old('city') }}" required>
                      @if ($errors->has('city'))
                      <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                      <label>Contact State</label>
                      <select class="form-control" id="state" name="state" required>
                        @foreach ($states as $state)
                        <option value="{{ $state->id }}"
                          @if (old('state') == $state->id) selected 
                          @endif>{{ $state->name }}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('state'))
                      <span class="help-block">
                        <strong>{{ $errors->first('state') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : '' }}">
                      <label>Contact ZIP code</label>
                      <input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Zip code" value="{{ old('zip_code') }}" required>
                      @if ($errors->has('zip_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('zip_code') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
                      <label>Contact Phone Number</label>
                      <input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number" value="{{ old('phone_number') }}" required>
                      @if ($errors->has('phone_number'))
                      <span class="help-block">
                        <strong>{{ $errors->first('phone_number') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                      <label>Contact Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}" >
                      @if ($errors->has('email'))
                      <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                      <label>Reason for Appointment/Details</label>
                      <textarea class="form-control" rows="4" id="description" placeholder="Reason for Appointment/Details..." name="description" form="appointment-form-{{ $loop->index }}"
                        required>@if(old('description')!==null){{ old('description') }}@endif</textarea> 
                      @if ($errors->has('description'))
                      <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="">
                  <button type="submit" class="btn btn-action text-black">Add Appointment</button>
                </div>
              </form>
              @endif
            </div>
            <!-- Appointment by Client -->
            @else
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title @if ($appointment->status) @if ($appointment->type == 0) text-black @else text-teal @endif @else text-red @endif">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="true"
                  aria-controls="collapse-{{ $loop->index }}">
                  {{ $appointment->start->format('g:i A') . ' ~ ' . $appointment->end->format('g:i A') }}
                  <span>{{ $appointment->title }}</span>
                </a>
              </h4>
            </div>
            <div id="collapse-{{ $loop->index }}" class="panel-collapse collapse @if ((old('active_id') === NULL && $appointment->start->format('U') == $timeStamp) || (old('active_id') !== NULL && $loop->index == old('active_id'))) in @endif" role="tabpanel" aria-labelledby="headingOne">
              @if ($appointment->client_id == Auth::user()->id)              
              <form action="{{ url('client/appointment/update') }}" method="post" id="appointment-form-{{ $loop->index }}" class="">
                {!! csrf_field() !!}
                <input type="hidden" name="active_id" id="active_id"  value="{{ $loop->index }}">
                <input type="hidden" class="appointment-id" name="id" id="id"  value="{{ $appointment->id }}">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label>Agency Name</label>
                      <p class="form-control">{{ $advisor->pivot->agency->name }}</p>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                      <label>Appointment Type</label>
                      <select class="form-control text" id="type" name="type" required
                      @if ($appointment->status==true) disabled @endif>
                        @foreach($types as $type)
                        <option value="{{ $loop->index }}"
                          @if (old('type')!==NULL)
                          @if(old('type')==$loop->index) selected @endif
                          @elseif ($appointment->type==$loop->index) selected
                          @endif>{{ $type }}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('type'))
                      <span class="help-block">
                        <strong>{{ $errors->first('type') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('setby') ? 'has-error' : '' }}">
                      <label>Appointment Set By:</label>
                      <input type="text" class="form-control" id="setby" name="setby" placeholder="Name" 
                      @if (old('setby')!==NULL) value="{{ old('setby') }}" 
                      @elseif ($appointment->setby !== null) value="{{ $appointment->setby }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      readonly>
                      @if ($errors->has('setby'))
                      <span class="help-block">
                        <strong>{{ $errors->first('setby') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                      <label>Contact Name</label>
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name"
                      @if (old('name')!==NULL) value="{{ old('name') }}" 
                      @elseif ($appointment->name !== null) value="{{ $appointment->name }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >
                      @if ($errors->has('name'))
                      <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                      <label>Contact Address</label>
                      <input type="text" class="form-control" id="address" name="address" placeholder="Address"
                      @if (old('address')!==NULL) value="{{ old('address') }}" 
                      @elseif ($appointment->address !== null) value="{{ $appointment->address }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >
                      @if ($errors->has('address'))
                      <span class="help-block">
                        <strong>{{ $errors->first('address') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('city') ? 'has-error' : '' }}">
                      <label>Contact Address</label>
                      <input type="text" class="form-control" id="city" name="city" placeholder="City"
                      @if (old('city')!==NULL) value="{{ old('city') }}" 
                      @elseif ($appointment->city !== null) value="{{ $appointment->city }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >
                      @if ($errors->has('city'))
                      <span class="help-block">
                        <strong>{{ $errors->first('city') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('state') ? 'has-error' : '' }}">
                      <label>Contact State</label>
                      <select class="form-control" id="state" name="state" required>
                        @foreach ($states as $state)
                        <option value="{{ $state->id }}"
                          @if (old('state') == $state->id) selected 
                          @elseif ($appointment->state == $state->name) selected 
                          @endif>{{ $state->name }}</option>
                        @endforeach
                      </select>
                      @if ($errors->has('state'))
                      <span class="help-block">
                        <strong>{{ $errors->first('state') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('zip_code') ? 'has-error' : '' }}">
                      <label>Contact Zip code</label>
                      <input type="text" class="form-control" id="zip_code" name="zip_code" placeholder="Zip code"
                      @if (old('zip_code')!==NULL) value="{{ old('zip_code') }}" 
                      @elseif ($appointment->zip_code !== null) value="{{ $appointment->zip_code }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >
                      @if ($errors->has('zip_code'))
                      <span class="help-block">
                        <strong>{{ $errors->first('zip_code') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('phone_number') ? 'has-error' : '' }}">
                      <label>Contact Phone Number</label>
                      <input type="tel" class="form-control" id="phone_number" name="phone_number" placeholder="Phone Number"
                      @if (old('phone_number')!==NULL) value="{{ old('phone_number') }}" 
                      @elseif ($appointment->phone_number !== null) value="{{ $appointment->phone_number }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >
                      @if ($errors->has('phone_number'))
                      <span class="help-block">
                        <strong>{{ $errors->first('phone_number') }}</strong>
                      </span>
                      @endif
                    </div>
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                      <label>Contact Email</label>
                      <input type="email" class="form-control" id="email" name="email" placeholder="Email"
                      @if (old('email')!==NULL) value="{{ old('email') }}" 
                      @elseif ($appointment->email !== null) value="{{ $appointment->email }}" 
                      @endif
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >
                      @if ($errors->has('email'))
                      <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                      <label>Reason for Appointment/Details</label>
                      <textarea class="form-control" rows="4" id="description" placeholder="Reason for Appointment/Details..." name="description" form="appointment-form-{{ $loop->index }}"
                      required 
                      {{--  @if ($appointment->status==true) readonly @endif  --}}
                      >@if(old('description')!==null){{ old('description') }}@else{{ $appointment->description }}@endif</textarea> 
                      @if ($errors->has('description'))
                      <span class="help-block">
                        <strong>{{ $errors->first('description') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                </div>
                {{--  @if ($appointment->status==false)  --}}
                <button type="submit" class="btn btn-action" style="margin-right: 16px;">Update</button>
                {{--  @endif  --}}
                <button type="button" class="btn btn-action appointment-remove">Remove</button>
              </form>
              @elseif ($advisor->clients()->find($appointment->client_id) != NULL && $advisor->clients()->find($appointment->client_id)->pivot->agency_id == $advisor->pivot->agency_id) 
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Agency Name</label>
                    <p class="form-control">{{ $advisor->clients()->find($appointment->client_id)->pivot->agency->name }}</p> 
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Appointment Type</label>
                    <p class="form-control">{{ $types[$appointment->type] }}</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Appointment Set By:</label>
                    <p class="form-control">{{ $appointment->setby }}</p>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Contact Name</label>
                    <p class="form-control">{{ $appointment->name }}</p>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Contact Address</label>
                    <p class="form-control">{{ $appointment->address }}</p>
                  </div>
                  <div class="form-group">
                    <label>Contact Address</label>
                    <p class="form-control">{{ $appointment->city }}</p>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Contact State</label>
                    <p class="form-control">{{ $appointment->state }}</p>
                  </div>
                  <div class="form-group">
                    <label>Contact Zip code</label>
                    <p class="form-control">{{ $appointment->zip_code }}</p>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Contact Phone Number</label>
                    <p class="form-control">{{ $appointment->phone_number }}</p>
                  </div>
                  <div class="form-group">
                    <label>Contact Email</label>
                    <p class="form-control">{{ $appointment->email }}</p>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label>Reason for Appointment/Details</label>
                    <textarea class="form-control" id="description" rows="4" readonly>{{ $appointment->description }}</textarea> 
                  </div>
                </div>
              </div>
              @endif
            </div>
            @endif
          </div>
          @empty
          @endforelse
        </div>
      </div>
    </div>
  </div>
</div>
@stop

@section('feature')
@stop

@push('js')
<script>
    $('select#type').change(function() {
      // console.log($(this).val())
      // console.log($(this).parents('div.row').next().find('select#length'))
      // if ($(this).val() != -1) {
        // $(this).parents('div.row').next().find('select#length').prop('disabled', false);
        if ($(this).val() == 0) {
          $(this).parents('form').find('select#length').val(3);
        } else if ($(this).val() == 1) {
          $(this).parents('form').find('select#length').val(1);
        }
        // $(this).parents('form').find('[type=submit]').removeAttr("disabled");
      // } else {
        // $(this).parents('div.row').next().find('select#length').prop('disabled', true);
        // $(this).parents('form').find('[type=submit]').attr("disabled", "disabled");
      // }
    });
    // $('select#length').change(function() {
    //   if ($(this).val() != -1) {
    //     $(this).parents('form').find('[type=submit]').removeAttr("disabled");
    //   } else {
    //     $(this).parents('form').find('[type=submit]').attr("disabled", "disabled");
    //   }
    // });
    var view_mode = '{{ $mode }}';
    console.log(view_mode);
    let timestamp = parseInt($('#timestamp').val());
    let advisor_id = $('#advisor_id').val();
    //Timepicker
    $('.start_time').timepicker({ 
      showInputs: false, 
      //defaultTime: '08:00 AM', 
      minuteStep: 15, 
      showMeridian: true,
      // maxHours: 24, 
    });
    // $('#appointment_date').datepicker({ 
    //     autoclose: true,
    //     startDate: 'today'
    // });
    // init calendar
    $('#calendar').fullCalendar({
      header : { left: '', center: '', right : '' }, 
      // header : { left : 'title', center: 'agendaDay,agendaWeek,month,listYear', right : 'prev,next today' }, 
      buttonText: { today: 'today', month: 'Month', week : 'Week', day : 'Day', list: 'Year' },
      defaultView: @if ($mode == 'week') 'agendaWeek' @elseif ($mode == 'day') 'agendaDay'  @elseif ($mode == 'year') 'listYear' @else '{{ $mode }}' @endif,
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!  
      eventLimit: true,
      displayEventEnd: true,
      scrollTime: '08:00:00',
      weekends: true,
      businessHours: {
        // days of week. an array of zero-based day of week integers (0=Sunday)
        // dow: [ 1, 2, 3, 4, 5 ], // Monday - Thursday
        // start: '08:00', // a start time (10am in this example)
        // end: '20:00', // an end time (6pm in this example)
      },
      timeFormat: 'h:mm A', // uppercase H for 24-hour clock
      views: {
        month: {
            // options apply to basicDay and agendaDay views
              eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
        },
        week: {
            // options apply to basicWeek and agendaWeek views
              eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
        },
        day: {
            // options apply to basicDay and agendaDay views
              eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
        }
      },
    //  timezone: 'America/Chicago',//'UTC',
    //  timezone: 'locale',
      // visibleRange: function(currentDate) {
      //   return {
      //     start: currentDate.clone().startOf('week'),
      //     end: currentDate.clone().add(1, 'months')
      //   };
      // },
      // validRange: function(nowDate) {
      //   return {
      //     start: moment(timestamp).startOf('week'),
      //     end: moment(timestamp).add(1, 'months')
      //   };
      // },
      // defaultDate: moment(timestamp * 1000),
      defaultDate: moment.unix(timestamp),
      events: function(start, end, timezone, callback) { // load events
        if (window.axios) {
            let advisor_id = $('#advisor_id').val();
          const url = '/client/appointment/search/' + advisor_id; 
          let timeoffset = new Date().getTimezoneOffset();
          const data = { 
            start: start.unix(),//format(), 
            end: end.unix(),//format(),
            timezone: timeoffset,
          };
          console.log(data)
          axios.post(url, data) 
          .then(function(response) {
            var events = [];
            console.log(response.data)
            if (layer && response.data.success) {
              for (let index in response.data.appointments) {
                events.push(response.data.appointments[index]);
              }
            }
            console.log(events);
            callback(events);
          }) 
          .catch(function(error) {
            callback([]);
            console.log(error.response.data)
            if (layer) layer.msg(error.response.data.message);
          });
        }
      },
      drop      : function (date, allDay) {
      },
      eventDrop: function(event, delta, revertFunc) {
          if (!confirm("Are you sure about this change?")) {
              revertFunc();
          } else {
            if (window.axios) {
              const url = '/client/appointment/update'; 
              const data = { 
                id: event.id,
                title: event.title, 
                start: event.start.unix(),
                end: event.end.unix(),
                setby: event.setby,
                name: event.name,
                address: event.address,
                city: event.city,
                phone_number: event.phone_number,
                email: event.email,
                // length: event.length,
                // type: event.type,
                status: event.status,
                description: event.description,
              }; 
              console.log(data)
              axios.post(url, data) 
              .then(function(response) {
                location.reload();
                if (layer && response.data.success) window.layer.msg('Updated Appointment');
              }) 
              .catch(function(error) {
                  revertFunc();
                console.log(error.response.data)
                if (layer) layer.msg(error.response.data.message);
              });
            }        
          }
      },
      eventResize: function(event, delta, revertFunc) {
          if (!confirm("is this okay?")) {
              revertFunc();
          } else {
              const url = '/client/appointment/update'; 
              const data = { 
                id: event.id,
                title: event.title, 
                start: event.start.unix(),
                end: event.end.unix(),
                setby: event.setby,
                name: event.name,
                address: event.address,
                city: event.city,
                phone_number: event.phone_number,
                email: event.email,
                // length: event.length,
                // type: event.type,
                status: event.status,
                description: event.description,
              }; 
            console.log(data)
            axios.post(url, data) 
            .then(function(response) {
              if (layer && response.data.success) window.layer.msg('Updated Appointment');
            }) 
            .catch(function(error) {
                  revertFunc();
                console.log(error.response.data)
                if (layer) layer.msg(error.response.data.message);
            });
          }
      },
      dayClick: function(date, jsEvent, view) {
        if (date.isSame(moment(), 'd')) {
          window.location = '/client/calendar/' + advisor_id + '?mode=' + view_mode;
        } else {
          timestamp = date.unix();
          window.location = '/client/calendar/' + advisor_id + '/' + timestamp + '?mode=' + view_mode;
        }
      },
      eventClick: function(event, element) {
        timestamp = event.start.unix();
        window.location = '/client/calendar/' + advisor_id + '/' + timestamp + '?mode=' + view_mode;
      },
      eventAfterAllRender: function( view ) {
        var time = moment();
        if (view_mode != 'year') return;
        if (time.unix() >= view.end.unix() || time.unix() <= view.start.unix()) {
          $('div.fc-scroller').animate({
              scrollTop: 0
            }, 0, function() {});
          return;
        }
        var st = time.format('YYYY-MM-DD');
        do {
          if ($('[data-date='+st+']').get(0)) {
            break;
          }
          if (time.unix() >= view.end.unix()) {
            return;
          }
          time.add(1, 'day');
          st = time.format('YYYY-MM-DD');
        } while (true);
        $('div.fc-scroller').animate({
            // scrollTop: $(`[data-date=${st}]`).offset().top
            scrollTop: $('[data-date='+st+']').position().top
          }, 100, function() {});
      }
    })
    $('#btn-today').click(function() {
      window.location = '/client/calendar/' + advisor_id + '?mode=' + view_mode;
    });
    $('#left-arrow').click(function() {
      console.log(timestamp);
      switch(view_mode) {
      case 'day':
        timestamp = moment.unix(timestamp).subtract(1, 'days').unix();
        break;
      case 'week':
        timestamp = moment.unix(timestamp).subtract(1, 'weeks').unix();
        break;
      case 'month':
        timestamp = moment.unix(timestamp).subtract(1, 'months').unix();
        break;
      case 'year':
        timestamp = moment.unix(timestamp).subtract(1, 'years').unix();
        break;
      }
      console.log(timestamp)
      window.location = '/client/calendar/' + advisor_id + '/' + timestamp + '?mode=' + view_mode;
    });
    $('#right-arrow').click(function() {
      console.log(timestamp) 
      switch(view_mode) {
      case 'day':
        timestamp = moment.unix(timestamp).add(1, 'days').unix();
        break;
      case 'week':
        timestamp = moment.unix(timestamp).add(1, 'weeks').unix();
        break;
      case 'month':
        timestamp = moment.unix(timestamp).add(1, 'months').unix();
        break;
      case 'year':
        timestamp = moment.unix(timestamp).add(1, 'years').unix();
        break;
      }
      console.log(timestamp)
      window.location = '/client/calendar/' + advisor_id + '/' + timestamp + '?mode=' + view_mode;
    });
    $('.btn-group .btn-action').click(function() {
      $('.btn-action').removeClass('active');
      $(this).addClass('active');
      view_mode = $(this).val();
      switch($(this).val()) {
        case 'day':
        $('#date-title').html(moment.unix(timestamp).format('MMM D, YYYY'))
        $('#calendar').fullCalendar('changeView', 'agendaDay');
        break;
        case 'week':
        $('#date-title').html(moment.unix(timestamp).format('MMM D, YYYY'))
        $('#calendar').fullCalendar('changeView', 'agendaWeek');
        break;
        case 'month':
        $('#date-title').html(moment.unix(timestamp).format('MMM YYYY'))
        $('#calendar').fullCalendar('changeView', 'month');
        break;
        case 'year':
        $('#date-title').html(moment.unix(timestamp).format('YYYY'))
        $('#calendar').fullCalendar('changeView', 'listYear');
        break;
      }
    });
    $('.appointment-remove').click(function() {
      if (confirm("Are you sure about this change?")) {
        if (window.axios) {
          const url = '/client/appointment/delete'; 
          let id = $(this).siblings('.appointment-id').val();
          const data = { 
            id: id,
          }; 
          console.log(data)
          axios.post(url, data) 
          .then(function(response) {
            location.reload();
            if (layer && response.data.success) window.layer.msg('Deleted Appointment');
          }) 
          .catch(function(error) {
            console.log(error.response.data)
            if (layer) layer.msg(error.response.data.message);
          });
        }        
      }
    })
    // setTimeout(function() {
    //   var ele = document.getElementsByClassName("in")[0].parentElement;
    //   console.log(ele)
    //   // ele.scrollIntoView({behavior: 'smooth'});
    //   var top = $('.in').position().top;
    //   console.log(top)
    //   // window.scrollTo(0, top - 120)
    //   // window.scroll(0, top - 120);
    //   window.scroll({
    //     top: top - 120,
    //     left: 0,
    //     behavior: 'smooth'
    //   })
    // }, 300);
    if ($('.in').get(0)) {
      $('html, body').animate({
        // scrollTop: $('.in').offset().top - 220
        scrollTop: $('.in').position().top - 120
      }, 500, function() {});
    }
    @if (session('success'))
      if (layer) window.layer.msg('{{ session('success') }}');
    @endif
</script>
@endpush