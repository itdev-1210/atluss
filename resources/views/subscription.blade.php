@extends('basic.page')

@section('title_postfix', 'Subscription')

@section('header')
<div class="container-fluid text-center">
    <div class="header_text">Welcome</div>
</div>
@stop

@section('banner')
<div class="container text-center">

</div>
@stop

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form text-center">
          <div class="heading">Choose Your Subscription</div>
          <form action="/subscribe" method="post" id="subscription-form" class="login-form">
              {{ csrf_field() }}
              <div class="form-group">
                  <input type="radio" class="form-control" id="check1" name="selector" value="1">
                  <label for="check1"> Monthly</label>
              </div>
              <div class="form-group">
                  <input type="radio" class="form-control" id="check" name="selector" value="2">
                  <label for="check"> Yearly</label>
              </div>
              <div class="form-group text-center">
                  <button type="submit" style="padding:8px 48px;">Next >></button>
              </div>
          </form>
        </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop