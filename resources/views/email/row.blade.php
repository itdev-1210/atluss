<tr style="display: table-row;">
    <td style="display: table-cell; border: solid 1px grey; overflow-wrap: break-word;">{{$label}}</td>
    <td style="display: table-cell; border: solid 1px grey; overflow-wrap: break-word; word-break: break-all;">{{$content}}</td>
</tr>