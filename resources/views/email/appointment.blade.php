@extends('email.basic')
@section('title')
<h2>Appointment Created</h2>
@stop
@section('header')
<p>{{ $stock }}</p>
@stop
@section('action')
@component('email.action')
    @slot('title')
    {{ $appointment->advisor->name }}'s Schdule
    @endslot
    @slot('url')
    {{ $url }}
    @endslot
@endcomponent
@stop
@section('content')
<h3>{{ $appointment->client->name }}'s Appointment</h3>
@stop
@section('detail')
@component('email.row')
    @slot('label')
    Appointment Time
    @endslot
    @slot('content')
    {{ $appointment->start }} ~ {{ $appointment->end }}
    @endslot
@endcomponent
@stop
@section('footer')
<p>Thank you for using our application!</p>
@stop