<style>
table {
    width: 100%;
}
.label {
    width: 20%;
}
.content {
    width: 80%;
}
</style>
<div style="width: 100%; text-align: center;">
    <div style="display: block; background: #f5f8fa; padding: 24px 0; font-size:24px; font-weight: bold; color: #74787e;">
        {{ config('app.name') }}
    </div>
    <div style="max-width: 800px; margin: 16px auto; text-align: left;">
        @yield('title')
        @yield('header')
        <div style="text-align: center;">
        @yield('action')
        </div>
        @yield('content')
        <table style="width: 100%; display: table;  border-collapse: collapse;" cellspacing="0" cellpadding="8px">
            <tbody style="display: table-row-group;">
            @yield('detail')
            </tbody>
        </table>
        @yield('footer')
    </div>
    <div style="display: block; background: #f5f8fa; padding: 32px 0; font-size:12px; font-weight: normal; color: #74787e;">
        &copy; 2018 {{ config('app.name') }}. All right Reserved
    </div>
</div>