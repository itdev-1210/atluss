@extends('template')

@section('title_postfix', 'Agency Setting')

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="login-form clearfix">
        {{--  <a class="btn btn-action btn-back" style="position: absolute;" href="{{ url('dashboard') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>  --}}
        <div class="heading text-center">Manage Agencies</div>
        <div class="clearfix">
          <button class="btn btn-action new_popup_open">Add New Agency</button>
        </div>
        <div id="new_popup" class="pricing-frame" style="display: none; width: 33%; padding: 2%;">
            <div class="clearfix">
                <h3 class="text-center" style="margin-bottom: 16px;">Add New Agency</h3>
                <form action="{{ url('agency/create') }}" method="post" id="new-form">
                    {!! csrf_field() !!}        
                    <div class="form-group">
                        <label for="name">Agency Name</label>
                        <input type="text" id="agency" name="agency" class="form-control" placeholder="Agency Name" required>
                    </div>
                    <button type="submit" class="btn btn-primary pull-left">Add</button>
                    <button type="reset" class="btn btn-danger new_popup_close pull-right">Cancel</button>
                </form>
            </div>       
        </div>
        <div id="edit_popup" class="pricing-frame" style="display: none; width: 33%; padding: 2%;">
          <div class="clearfix">
            <h3 class="text-center" style="margin-bottom: 16px;">Edit Agency</h3>
            <form action="{{ url('agency/edit') }}" method="post" id="edit-form">
              {!! csrf_field() !!}  
              <input type="hidden" id="id" name="id"/>   
              <div class="user_list" style="max-height: 30vh; width: 100%; overflow: auto; margin-bottom: 16px;">      
                @forelse ($clients as $client)
                <div style="width: 100%">
                  <input id="{{ $client->id }}" type="checkbox" name="clients[]" value="{{ $client->id }}" />
                  <label for="{{ $client->id }}">{{ $client->name }}</label>
                </div>
                @empty
                @endforelse
              </div>
              <button type="submit" class="btn btn-primary pull-left">Update</button>
              <button type="reset" class="btn btn-danger edit_popup_close pull-right">Cancel</button>
            </form>
          </div>         
        </div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          @forelse($agencies as $agency)
          <div class="panel panel-warning">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ $loop->index }}" aria-expanded="false"
                  aria-controls="collapse-{{ $loop->index }}">
                  {{ $agency->name }} - {{ $agency->advisor_client_group_count }} Users, {{ $agency->invites_count }} Invites
                </a>
              </h4>
            </div>
            <div id="collapse-{{ $loop->index }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone">
              <div class="panel-body">
                <form id="agency-form-{{ $loop->index }}" action="{{ url('agency/update') }}" method="POST">
                  {{ csrf_field() }}
                  <input type="hidden" id="id" name="id" value="{{ $agency->id }}"/>   
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="Agency Name" id="name" name="name" value="{{ $agency->name }}" required>
                  </div>
                  @if ($agency->advisor_client_group_count > 0 || $agency->invites_count > 0)
                  <div id="detail{{ $agency->id }}" class="collapse" style="margin-top:16px;">
                    @forelse($agency->advisor_client_group as $advisor_client)
                    <p>{{ $advisor_client->client->name }}</p>
                    @empty
                    @endforelse
                    @forelse($agency->invites as $invie)
                    <p class="text-red">{{ $invie->email }}</p>
                    @empty
                    @endforelse
                  </div>               
                  <button type="button" class="btn btn-action" data-toggle="collapse" data-target="#detail{{ $agency->id }}" style="margin-right: 16px;">More Details</button>
                  @endif
                  <button type="submit" class="btn btn-action" style="margin-right: 16px;">Update</button>
                  <button type="button" class="btn btn-action edit-button" style="margin-right: 16px;">Edit</button>
                  @if ($agency->advisor_client_group_count == 0 && $agency->invites_count == 0)
                  <button type="button" class="btn btn-action remove-button text-red" value="user">Remove</button>
                  @endif
                </form>
              </div>
            </div>
          </div>
          @empty
          @endforelse
        </div>
      </div>
    </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
<script>
  let clients = @json($clients);
  $('#new_popup').popup({
    beforeopen: function () {
      $('#new_popup #agency').val('');
    }
  });
  $('#edit_popup').popup();
  $('.edit-button').click(function() {
    let id = $(this).siblings('#id').val();
    console.log(id);
    $('#edit_popup .user_list').children().remove();
    $('#edit_popup .user_list').siblings('#id').val(id);
    $.each(clients, function(index, client) {
      console.log(client);
      if (client.pivot.agency_id != id) {
        $("<div style='width: 100%;'>\
            <input id='" + client.id  + "' type='checkbox' name='clients[]' value='" + client.id + "' />\
            <label for='" + client.id + "'>" + client.name + "</label>\
          </div>").appendTo('#edit_popup .user_list');
      }
    })
    $('#edit_popup').popup('show');
  })
  $('.remove-button').click(function() { 
    let url = '/agency/remove'; 
    if (layer) layer.confirm("Are you sure delete this Agency?", {
      title: 'Delete',
      btn: ["Yes", "No"]
    }, function(){
      if (window.axios) {
        let id = $(this).siblings('#id').val();
        const data = { 
          id: id,
        }; 
        let self = $(this);
        axios.post(url, data) 
        .then(function(response) {
          // location.reload();
          if (layer && response.data.success) window.layer.msg('Removed');
          self.parents('.panel').remove();
        }) 
        .catch(function(error) {
          console.log(error.response.data)
          if (layer) layer.msg(error.response.data.message);
        });
      }
    }, function(){
      
    }); 
  })
</script>
@stop