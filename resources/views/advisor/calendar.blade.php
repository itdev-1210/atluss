@extends('template')

@push('css')
<style>
  .available td {
    color: #762696; 
  }
  .unavailable td {
    color: #be0000; 
  }
  .phone td {
    color: #00cced; 
  }
  .office td {
    color: #000000; 
  }
</style>
@endpush

@section('title_postfix', 'Calendar')

@section('content')
<div class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="calendar-form">
        <div class="text-center">
          <ul class="events pull-right">
              <li>Office Meeting Scheduled</li>
              <li>Unavailable for Appointment</li>
              <li>Available for Appointment</li>
              <li>Phone Meeting Scheduled</li>
          </ul>
        </div>
        <div class="heading text-center">
          @isset($user) {{ $user->name }}'s Calendar @endisset @empty($user) My Calendar @endempty
          <br>
          <div class="text-center btn-group">
            <button type="button" class="btn btn-action {{$calendar_setting == 1 ? 'active': ''}}" value="day">Day</button>
            <button type="button" class="btn btn-action {{$calendar_setting == 2 ? 'active': ''}}" value="week">Week</button>
            <button type="button" class="btn btn-action {{$calendar_setting == 3 ? 'active': ''}}" value="month">Month</button>
            <button type="button" class="btn btn-action {{$calendar_setting == 4 ? 'active': ''}}" value="year">Year</button>
          </div>
          <input type="hidden" id="user_id" name="user_id" @isset($user) value="{{ $user->id }}" @endisset />  
        </div>
        <!-- THE CALENDAR -->
        <div id="calendar"></div>
      </div>
    </div>
  </div>
</div>
@stop

@section('feature')
@stop

@push('js')
<script>
// init calendar
var calendar_setting = "{{$calendar_setting}}";
var mode = 'year';
var defaultView = 'listYear';
if (calendar_setting == 1) {
  defaultView = 'agendaDay';
  mode = 'day';
} else if (calendar_setting == 2) {
  defaultView = 'agendaWeek';
  mode = 'week';
} else if (calendar_setting == 3) {
  defaultView = 'month';
  mode = 'month';
}
var date = new Date() 
var d = date.getDate(), m = date.getMonth(), y = date.getFullYear() 
const user_id = $('#user_id').val();
$('#calendar').fullCalendar({
  header : { left: 'title', center: '', right : 'prev,next today' }, 
  // header : { left : 'title', center: 'agendaDay,agendaWeek,month,listYear', right : 'prev,next today' }, 
  buttonText: { today: 'today', month: 'Month', week : 'Week', day : 'Day', list: 'Year' },
  defaultView: defaultView,  
  editable  : true,
  droppable : true, // this allows things to be dropped onto the calendar !!!  
  eventLimit: true,
  displayEventEnd: true,
  scrollTime: '08:00:00',
  weekends: true,
  businessHours: {
    // days of week. an array of zero-based day of week integers (0=Sunday)
    // dow: [ 1, 2, 3, 4, 5 ], // Monday - Thursday
    start: '08:00', // a start time (10am in this example)
    end: '20:00', // an end time (6pm in this example)
  },
  timeFormat: 'h:mm A', // uppercase H for 24-hour clock
  views: {
    month: {
        // options apply to basicDay and agendaDay views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    week: {
        // options apply to basicWeek and agendaWeek views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    day: {
        // options apply to basicDay and agendaDay views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    agendaFourDay: {
      type: 'agenda',
      duration: { days: 4 },
      buttonText: '4 day'
    }
  },
  // validRange: function(nowDate) {
  //   return {
  //     start: nowDate.clone().startOf('week'),
  //     end: nowDate.clone().add(1, 'months')
  //   };
  // },
  events: function(start, end, timezone, callback) { // load events
    if (window.axios) {
      const url = user_id ? '/appointment/search/' + user_id : '/appointment/search'; 
      let timeoffset = new Date().getTimezoneOffset();
      const data = { 
        start: start.unix(), 
        end: end.unix(),
        timezone: timeoffset,
      }; 
      axios.post(url, data)
      .then(function(response) {
        var events = [];
        if (layer && response.data.success) {
          console.log(response.data.appointments);
          for (let index in response.data.appointments) {
            events.push(response.data.appointments[index]);
          }
        }
        console.log(events);
        callback(events);
      }) 
      .catch(function(error) { 
        callback([]);
        console.log(error.response.data)
        if (layer) layer.msg(error.response.data.message);
      });
    }
  },
  drop      : function (date, allDay) {
  },
  eventDrop: function(event, delta, revertFunc) {
      if (!confirm("Are you sure about this change?")) {
          revertFunc();
      } else {
        if (window.axios) {
          const url = '/appointment/update'; 
          const data = { 
            id: event.id,
            title: event.title, 
            start: event.start.unix(),
            end: event.end.unix(),
            setby: event.setby,
            name: event.name,
            address: event.address,
            city: event.city,
            phone_number: event.phone_number,
            email: event.email,
            // length: event.length,
            // type: event.type,
            status: event.status,
            description: event.description,
          }; 
          console.log(data)
          axios.post(url, data) 
          .then(function(response) {
            location.reload();
            if (layer && response.data.success) window.layer.msg('Updated Appointment');
          }) 
          .catch(function(error) { 
              revertFunc();
            console.log(error.response.data)
            if (layer) layer.msg(error.response.data.message);
          });
        }        
      }
  },
  eventResize: function(event, delta, revertFunc) {
      if (!confirm("is this okay?")) {
          revertFunc();
      } else {
        const url = '/appointment/update'; 
          const data = { 
            id: event.id,
            title: event.title, 
            start: event.start.unix(),
            end: event.end.unix(),
            setby: event.setby,
            name: event.name,
            address: event.address,
            city: event.city,
            phone_number: event.phone_number,
            email: event.email,
            // length: event.length,
            // type: event.type,
            status: event.status,
            description: event.description,
          }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          if (layer && response.data.success) window.layer.msg('Updated Appointment');
        }) 
        .catch(function(error) { 
              revertFunc();
            console.log(error.response.data)
            if (layer) layer.msg(error.response.data.message);
        });
      }
  },
  dayClick: function(date, jsEvent, view) {
    if (date.isSame(moment(), 'd')) {
      window.location = user_id ? '/client/calendar/' + user_id : '/schedule';
    } else {
      timestamp = date.unix();
      window.location = user_id ? '/client/calendar/' + user_id + '/' + timestamp : '/schedule/' + timestamp;
    }
  },
  eventClick: function(event, element) {
    var timestamp = event.start.unix();
    window.location = user_id ? '/client/calendar/' + user_id + '/' + timestamp : '/schedule/' + timestamp;
  },
  eventAfterAllRender: function( view ) {
    var time = moment();
    console.log(view.end)
    if (mode != 'year') return;
    if (time.unix() >= view.end.unix() || time.unix() <= view.start.unix()) {
      $('div.fc-scroller').animate({
          scrollTop: 0
        }, 0, function() {});
      return;
    }
    var st = time.format('YYYY-MM-DD');
    do {
      if ($('[data-date='+st+']').get(0)) {
        break;
      }
      if (time.unix() >= view.end.unix()) {
        return;
      }
      time.add(1, 'day');
      st = time.format('YYYY-MM-DD');
    } while (true);
    console.log(st)
    $('div.fc-scroller').animate({
        // scrollTop: $(`[data-date=${st}]`).offset().top
        scrollTop: $('[data-date='+st+']').position().top
      }, 100, function() {});
  }
})
$('.btn-group .btn-action').click(function() {
  $('.btn-action').removeClass('active');
  $(this).addClass('active');
  switch($(this).val()) {
    case 'day':
    mode = 'day';
    $('#calendar').fullCalendar('changeView', 'agendaDay');
    break;
    case 'week':
    mode = 'week';
    $('#calendar').fullCalendar('changeView', 'agendaWeek');
    break;
    case 'month':
    mode = 'month';
    $('#calendar').fullCalendar('changeView', 'month');
    break;
    case 'year':
    mode = 'year';
    $('#calendar').fullCalendar('changeView', 'listYear');
    break;
  }
})
</script>
@endpush