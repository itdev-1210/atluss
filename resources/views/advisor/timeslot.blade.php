@extends('template')

@section('title_postfix', 'Timeslot')

@push('css')
<style>
  .available {
    background: rgba(118,38,150,0.5);
  }
  .unavailable {
    background: rgba(190,0,0,0.5);
  }
  .btn.active {
    background-color: #a8d1ff;
    color: #0073b7;
  }
</style>
@endpush

@section('content')
<section class="section-atluss">
  <div class="container">
    <div class="pricing-frame">
      <div class="">
        <a class="btn btn-action btn-back" style="position: absolute; margin-left: 16px;" href="{{ url('dashboard') }}"><span class="fc-icon fc-icon-left-single-arrow"></span></a>
        <div class="heading">My Time Slots</div>
        <div class="flex-container">
          @for ($i = 0; $i < 7; $i++)
          <div class="weekday" value="{{ $i }}">
            <h4>{{ $weekday[$i] }}</h4>
            <button class="btn btn-action @if ($i==1) active @endif">Add Time</button>
            @isset($timeslots[$i])
            @foreach($timeslots[$i] as $timeslot)
            <div class="time-slot">
              <div class="text-center @if ($timeslot['type']) available @else unavailable @endif">
                <p>{{ $timeslot['start_time'] }}</p>
                <p>{{ $timeslot['end_time'] }}</p>
                <p>@if ($timeslot['type']) Available @else Unavailable @endif</p>
              </div>
              <a href="javascrip:void(0)" class="" value="{{ $timeslot['id'] }}">
                <span class="glyphicon glyphicon-remove" style="color:red"></span>
              </a>
            </div>
            @endforeach
            @endisset
            @if ($i == 1)            
            <div class="time-slot new">
              <div class="text-center time">
                <input class="text-center form-control" type="text" id="start_time" name="start_time" placeholder="08:00 AM">
                <input class="text-center form-control" type="text" id="end_time" name="end_time" placeholder="10:00 AM">
                <select class="form-control" id="type" name="type" required>
                  <option value="1">Available</option>
                  <option value="0">Unavailable</option>
                </select>
              </div>
              <a href="javascript:void(0)" class="">
                <span class="glyphicon glyphicon-plus" style="color:#00cced"></span>
              </a>
            </div>
            @endif
          </div>
          @endfor
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
@stop

@section('feature')
@stop

@section('js')
<script>
  var initTimepicker = function() {
    //Timepicker
    $('#start_time').timepicker({ 
      showInputs: false, 
      defaultTime: '08:00 AM', 
      minuteStep: 5, 
      showMeridian: true, 
      // maxHours: 24, 
    });
    $('#end_time').timepicker({ 
      showInputs: false, 
      defaultTime: '10:00 AM', 
      minuteStep: 5, 
      showMeridian: true, 
      // maxHours: 24, 
    });
    $('#end_time').timepicker().on('show.timepicker', function(e) {
      $('#start_time').timepicker('hideWidget')
    })
    $('#start_time').timepicker().on('show.timepicker', function(e) {
      $('#end_time').timepicker('hideWidget')
    })
    $('#start_time').timepicker().on('changeTime.timepicker', function(e) {
      console.log(e.time.value)
      var start_time = moment(e.time.value, 'H:mm A');
      var end_time = moment($('#end_time').val(), 'H:mm A');
      if (start_time >= end_time) {
        $('#end_time').timepicker('setTime', moment(e.time.value, 'H:mm A').add(1, 'hour').format('H:mm A'))
      }
    })
    $('#end_time').timepicker().on('changeTime.timepicker', function(e) {
      console.log(e.time.value)
      var end_time = moment(e.time.value, 'H:mm A');
      var start_time = moment($('#start_time').val(), 'H:mm A');
      if (start_time >= end_time) {
        $('#start_time').timepicker('setTime', moment(e.time.value, 'H:mm A').subtract(1, 'hour').format('H:mm A'))
      }
    })
  }
  let createTimeslot = function(element) {
    let start_time = element.siblings('.time').children('#start_time').val();
    let end_time = element.siblings('.time').children('#end_time').val();
    let type = element.siblings('.time').children('#type').val();
    let weekday = element.parents('.weekday').attr('value');
    let timezone = new Date().getTimezoneOffset();
    console.log(timezone)
    const url = '/timeslot/create'; 
    const data = { 
      weekday: weekday, 
      start_time: start_time, 
      end_time: end_time, 
      type: type, 
      timezone: timezone 
    }; 
    console.log(data)
    if (window.axios) {
        axios.post(url, data) 
        .then(function(response) {
          console.log(response.data.timeslot)
          let timeslot = response.data.timeslot;
            if (layer && response.data.success) {
              window.layer.msg('Created timeslot');
              let status = type == 1 ? 'Available' : 'Unavailable';
              let classname = type == 1 ? 'available' : 'unavailable';
              let new_time = $('<div class="time-slot">\
                  <div class="text-center ' + classname + '">\
                    <p>' + start_time + '</p>\
                    <p>' + end_time + '</p>\
                    <p>' + status + '</p>\
                  </div>\
                  <a href="javascript:void(0)" class="" value="' + timeslot.id + '">\
                    <span class="glyphicon glyphicon-remove" style="color:red"></span>\
                  </a>\
              </div>');
              new_time.insertBefore(element.parent());
              new_time.on('click', 'a', function(){
                deleteTimeslot($(this))
              });
              // location.reload();
            }
        }) 
        .catch(function(error) {
          console.log(JSON.stringify(error.response.data));
            if (layer) layer.msg('Sorry, you can\'t add a timeslot there.');//error.response.data.message);
        });
    }
  }
  let deleteTimeslot = function(element) {
    let id = element.attr('value');
    const url = '/timeslot/delete'; 
    const data = { id: id };
    console.log(data)
    if (window.axios) {
        axios.post(url, data) 
        .then(function(response) {
          console.log(response.data.timeslot)
            if (layer && response.data.success) window.layer.msg('Deleted timeslot');
            element.parents('.time-slot').remove();
        }) 
        .catch(function(error) {
          console.log(JSON.stringify(error.response.data));
            if (layer) layer.msg(error.response.data.message);
        });
    }
  }
  $('.weekday .btn').click(function() {
    $('.weekday .btn').removeClass('active');
    $(this).addClass('active');
    let newElement = $('.time-slot.new');
    $('.time-slot.new').remove();
    $(this).parent('.weekday').append(newElement)
    newElement.children('a').click(function() {
      createTimeslot($(this))
    })
    initTimepicker();
    {{--  $(this).parent('.weekday').append("<div class='time-slot new'>\
      <div class='text-center time'>\
        <input class='text-center form-control' type='text' id='start_time' name='start_time' placeholder='08:00 AM'>\
        <input class='text-center form-control' type='text' id='end_time' name='end_time' placeholder='10:00 AM'>\
        <select class='form-control' id='type' name='type' required>\
          <option value='1'>Available</option>\
          <option value='0'>Unavailable</option>\
        </select>\
      </div>\
      <a href='javascrip:void(0)' class=''>\
        <span class='glyphicon glyphicon-plus' style='color:#00cced'></span>\
      </a>\
    </div>")  --}}
    //alert('clicked');
  })
  $('.time-slot a').click(function() {
    console.log('**********')
    if ($(this).parent('.time-slot').hasClass('new')) {
      createTimeslot($(this))
    } else {
      deleteTimeslot($(this))
    }
  });
  initTimepicker();
</script>
@stop