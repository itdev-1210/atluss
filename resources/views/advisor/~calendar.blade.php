@extends('template')

@push('css')
@endpush

@section('title_postfix', 'Calendar')

@section('content')
<div class="section-atluss">
  <div class="container">
    <div class="pricing-frame clearfix">
      <div class="heading text-center">
        My Calendar
        <br>
        <div class="text-center btn-group">
          <button type="button" class="btn btn-action" value="day">Day</button>
          <button type="button" class="btn btn-action" value="week">Week</button>
          <button type="button" class="btn btn-action active" value="month">Month</button>
          <button type="button" class="btn btn-action" value="year">Year</button>
        </div>
      </div>
      <div class="col-md-3">
        <h4 class="text-center">Events</h4>
        <!-- the events -->
        <div id="external-events">
          <ul>
            <li class="text-green">Office meeting scheduled</li>
            <li class="text-yellow">Unavailable for appointment</li>
            <li class="text-aqua">Available for appointment</li>
            <li class="text-light-blue">Phone meeting scheduled</li>
            <li class="text-red">Sleep tight</li>
          </ul>
          <div class="input-group">
            <div class="col-xs-6">
              <div class="row">
                <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                  </div>
                  <input type="text" class="form-control" id="start_time" placeholder="08:00" name="start_time">
                </div>
              </div>
            </div>
            <div class="col-xs-6">
              <div class="row">
                <div class="input-group">
                  <span class="input-group-addon">~</span>
                  <input type="text" class="form-control" id="end_time" placeholder="10:00" name="end_time">
                </div>
              </div>
            </div>
          </div>
          <div class="checkbox">
            <label for="drop-remove">
                    <input type="checkbox" id="drop-remove">
                      Remove after drop
                    </label>
          </div>
        </div>
        <h4 class="text-center">Create Event</h4>
        <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
          <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
          <ul class="fc-color-picker" id="color-chooser">
            <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
            <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
          </ul>
        </div>
        <!-- /btn-group -->
        <div class="input-group">
          <input id="new-event" type="text" class="form-control" placeholder="Event Title">
          <div class="input-group-btn">
            <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>
          </div>
          <!-- /btn-group -->
        </div>
        <!-- /input-group -->
      </div>
      <!-- /.col -->
      <div class="col-md-9">
        <!-- THE CALENDAR -->
        <div id="calendar"></div>
      </div>
      <!-- /.col -->
      <div class="panel-group login-form" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-warning">
            <div class="panel-heading" role="tab" id="headingOne">
              <h4 class="panel-title">
                <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse_new" aria-expanded="false"
                  aria-controls="collapse_new">
                  New Appointment
                </a>
              </h4>
            </div>
            <div id="collapse_new" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingone">
              <div class="panel-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute,
                non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor,
                sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh
                helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher
                vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably have
                heard of them accusamus labore sustainable VHS.
                <br/>
              </div>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@stop

@section('feature')
@stop

@push('js')
<script>
//Timepicker
$('#start_time').timepicker({ 
  showInputs: false, 
  defaultTime: '08:00 AM', 
  minuteStep: 5, 
  showMeridian: true,
  maxHours: 24, 
});
$('#end_time').timepicker({ 
  showInputs: false, 
  defaultTime: '10:00 AM', 
  minuteStep: 5, 
  showMeridian: true, 
  maxHours: 24, 
});
/* initialize the external events -----------------------------------------------------------------*/ 
function init_events(ele) { 
  ele.each(function () { 
    // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/) 
    // it doesn't need to have a start or end 
    var eventObject = { 
      title: $.trim($(this).text()) // use the element's text as the event title
    } 
    // store the Event Object in the DOM element so we can get to it later 
    $(this).data('eventObject', eventObject) 
    // make the event draggable using jQuery UI 
    $(this).draggable({ 
      zIndex : 1070, 
      revert : true,  // will cause the event to go back to its 
      revertDuration: 0 // original position after the drag 
    }) 
  }) 
} 
init_events($('#external-events li'));

// init calendar
var date = new Date() 
var d = date.getDate(), m = date.getMonth(), y = date.getFullYear() 
$('#calendar').fullCalendar({
  header : { left: 'title', center: '', right : 'prev,next today' }, 
  // header : { left : 'title', center: 'agendaDay,agendaWeek,month,listYear', right : 'prev,next today' }, 
  buttonText: { today: 'today', month: 'Month', week : 'Week', day : 'Day', list: 'Year' },
  defaultView: 'month',  
  editable  : true,
  droppable : true, // this allows things to be dropped onto the calendar !!!  
  eventLimit: true,
  displayEventEnd: true,
  weekends: true,
  businessHours: {
    // days of week. an array of zero-based day of week integers (0=Sunday)
    dow: [ 1, 2, 3, 4, 5 ], // Monday - Thursday
    start: '08:00', // a start time (10am in this example)
    end: '20:00', // an end time (6pm in this example)
  },
  timeFormat: 'H:mm', // uppercase H for 24-hour clock
  views: {
    month: {
        // options apply to basicDay and agendaDay views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    week: {
        // options apply to basicWeek and agendaWeek views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    },
    day: {
        // options apply to basicDay and agendaDay views
          eventLimit: 4 // adjust to 6 only for agendaWeek/agendaDay
    }
  },
  events: function(start, end, timezone, callback) { // load events
    if (window.axios) {
      const url = '/appointment/search'; 
      const data = { 
        start: start.format(), 
        end: end.format(),
        timezone: timezone,
      }; 
      axios.post(url, data) 
      .then(function(response) {
        var events = [];
        if (layer && response.data.success) {
          console.log(response.data.appointments);
          //window.layer.msg('Created new Appointment');
          for (let event of response.data.appointments) {
            events.push(event);
          }
        }
        console.log(events);
        callback(events);
      }) 
      .catch(function(error) {
        callback([]);
        if (layer) layer.msg(error.data);
      });
    }
  },
  drop      : function (date, allDay) { // this function is called when something is dropped
    // retrieve the dropped element's stored Event Object
    var originalEventObject = $(this).data('eventObject')
    // we need to copy it, so that multiple events don't have a reference to the same object
    var copiedEventObject = $.extend({}, originalEventObject)
    var start_time = $('#start_time').val().split(":");
    var end_time = $('#end_time').val().split(":");
    var start_hour = start_time[0];
    var start_minute = start_time[1];
    var end_hour = end_time[0];
    var end_minute = end_time[1];
    var start_date = date;
    var end_date = date.clone();
    start_date.utc().hours(start_hour);
    start_date.utc().minutes(start_minute);
    end_date.utc().hours(end_hour);
    end_date.utc().minutes(end_minute);
    console.log(start_date)
    console.log(end_date)
    console.log('originalEventObject')
    console.log(originalEventObject)
    console.log('copiedEventObject')
    console.log(copiedEventObject)
    console.log(date)
    console.log(allDay)
    // assign it the date that was reported
    copiedEventObject.start           = start_date
    copiedEventObject.end           = end_date
    copiedEventObject.allDay          = allDay
    copiedEventObject.backgroundColor = $(this).css('background-color')
    copiedEventObject.borderColor     = $(this).css('border-color')
    copiedEventObject.textColor     = $(this).css('color')
    if (window.axios) {
      const url = '/appointment/create'; 
      const data = { 
        title: copiedEventObject.title, 
        start: copiedEventObject.start.format(),
        end: copiedEventObject.end ? copiedEventObject.end.format() : null,
        allDay: true,//copiedEventObject.allDay,
        backgroundColor: copiedEventObject.backgroundColor,
        borderColor: copiedEventObject.borderColor,
        textColor: copiedEventObject.textColor,
      }; 
      console.log(data)
      axios.post(url, data) 
      .then(function(response) {
          if (layer && response.data.success) {
            copiedEventObject.id = response.data.appointment
            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true)
            window.layer.msg('Created new Appointment');
          }
      }) 
      .catch(function(error) {
        console.log(error.data)
          if (layer) layer.msg(error.data);
      });
    }  
    // is the "remove after drop" checkbox checked?
    if ($('#drop-remove').is(':checked')) {
      // if so, remove the element from the "Draggable Events" list
      $(this).remove()
    }
  },
  eventDrop: function(event, delta, revertFunc) {
      if (!confirm("Are you sure about this change?")) {
          revertFunc();
      } else {
        if (window.axios) {
          const url = '/appointment/update'; 
          const data = { 
            id: event.id,
            title: event.title, 
            start: event.start.format(),
            end: event.end ? event.end.format() : null,
            allDay: event.allDay,
            backgroundColor: event.backgroundColor,
            borderColor: event.borderColor,
            textColor: event.textColor,
          }; 
          console.log(data)
          axios.post(url, data) 
          .then(function(response) {
            if (layer && response.data.success) window.layer.msg('Updated Appointment');
          }) 
          .catch(function(error) {
            if (layer) layer.msg(error.data);
          });
        }        
      }
  },
  eventResize: function(event, delta, revertFunc) {
      if (!confirm("is this okay?")) {
          revertFunc();
      } else {
        const url = '/appointment/update'; 
        const data = { 
          id: event.id,
          title: event.title, 
          start: event.start.format(),
          end: event.end ? event.end.format() : null,
          allDay: event.allDay,
          backgroundColor: event.backgroundColor,
          borderColor: event.borderColor,
          textColor: event.textColor,
        }; 
        console.log(data)
        axios.post(url, data) 
        .then(function(response) {
          if (layer && response.data.success) window.layer.msg('Updated Appointment');
        }) 
        .catch(function(error) {
          if (layer) layer.msg(error.data);
        });
      }
  },
  dayClick: function() {
      //alert('a day has been clicked!');
  },
  eventClick: function(event, element) {
    //alert('a event has been clicked!');
  }
})
/* ADDING EVENTS */
//Color chooser button
var colorChooser = $('#color-chooser-btn')
$('#color-chooser > li > a').click(function (e) {
  e.preventDefault()
  //Save color
  currColor = $(this).css('color')
  currClass = $(this).attr('class')
  //Add color effect to button
  $('#add-new-event').css({ 'background-color': currColor, 'border-color': currColor })
})
$('#add-new-event').click(function (e) {
  e.preventDefault()
  //Get value and make sure it is not null
  var val = $('#new-event').val()
  if (val.length == 0) {
    return
  }

  //Create events
  var event = $('<li />')
  event.addClass(currClass)
  event.html(val)
  $('#external-events ul').prepend(event)

  //Add draggable funtionality
  init_events(event)

  //Remove event from text input
  $('#new-event').val('')
})
$('.btn-action').click(function() {
  $('.btn-action').removeClass('active');
  $(this).addClass('active');
  switch($(this).val()) {
    case 'day':
    $('#calendar').fullCalendar('changeView', 'agendaDay');
    break;
    case 'week':
    $('#calendar').fullCalendar('changeView', 'agendaWeek');
    break;
    case 'month':
    $('#calendar').fullCalendar('changeView', 'month');
    break;
    case 'year':
    $('#calendar').fullCalendar('changeView', 'listYear');
    break;
  }
})
</script>
@endpush