<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->text('daily_content')->nullable();
            $table->time('daily_time')->nullable();
            $table->text('appointment_content')->nullable();
            $table->tinyInteger('prior_time')->default(24);
            $table->text('reminder_content')->nullable();
            $table->smallInteger('timezone')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_settings');
    }
}
